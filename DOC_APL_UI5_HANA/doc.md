# Entwicklung einer SAPUI5-Web-Applikation mit Anbindung an eine SAP-HANA-Express-Datenbank über einen ESH-REST-Service

## 0. System und relevante Tools

Die Entwicklung der App fand hauptsächlich auf [Ubnutu 17.04](http://releases.ubuntu.com/17.04/) unter Zuhilfenahme der IDE Webstorm von [JetBrains](https://www.jetbrains.com/webstorm/) statt. <b>Hinweis:</b> JetBrains IDEs empfehlen sich sehr gut und werden immer mehr zum Standard in der Software-Entwicklung bei vielen Firmen. Studenten-Accounts sind kostenfrei.<br> Die [SAP Web IDE](https://www.sap.com/developer/topics/sap-webide.html) - dient als SAPs Standard-Tool für die Entwicklung von Fiori- und UI5-Apps. Für eine schnelle Zusammenstellung eines Frontend-Designs wird auf https://www.build.me verwiesen, ebenso kann dort auch entsprechend Code per WYSIWYG generiert werden.<br>
Eine virtuelles Ubuntu 16.04 - Gnome Flashback Metacity - über VMWare steht zur Verfügung. Die Ordnerstruktur des Projekts stammt aus dem Download eines SAPWebIDE-Projekts, worauf im weiteren Verlauf eingegangen wird. Eine MySQL-Testdatenbank soll als Ersatz für eine HANA-Express-DB dienen, diese ebenso auf dem OS installiert und eingerichtet ist. Ebenfalls wichtige Resourcen für die Verwendung der SAP-Module für die UI5-Applikation findet man auf [OpenUI5.org](http://openui5.org/download.html). <br><br>
Anmeldung auf Ubuntu:<br>
User: ui5user<br>
Passwort: ui5<br><br>
Die aktuelle Applikation ist auf [GitLab/s72898](https://gitlab.com/s72898/UI5HANA_APL_v0.2) zu finden und öffentlich zugänglich. Mit
<code>
git clone https://gitlab.com/s72898/UI5HANA_APL_v0.2.git
</code>
kann die App zzgl. weiterer Inhalte heruntergeladen werden. Im Verzeichnis <code>/home/UI5HANA_APL_v0.2</code> findet man die Git-Repository wieder.
Node.js, Nodejs-Legacy, Nodemon und NPM sind ebenfalls installiert. 

## 1.0 Getting Started

Dieses Kapitel kann gern übersprungen werden, da die Projektordner inklusive manifest.json angepasst wurden. Weiter geht es in <b>Kapitel 1.3.</b><br> Um jedoch nachvollziehen zu können, wie die Projektstruktur zustande kommt, wird dies kurz erklärt. Außerdem wird die Arbeit unter der WebIDE als von SAP empfohlen, da sie mittlerweile zur SAP Cloud Platform gehört und neben UI5 auch Hana- und XS(A)-Logik über Services programmiert und konfiguriert wird.

### 1.1 Installieren des Projektes

Nach der Registrierung und Anmeldung bei der SAP Web IDE wird "Europe (Rot) - Trial" > p-XXXX...trial ausgewählt. ![SAP Web IDE](sapwebidelink.png) Auf der linken Seite, der Sidebar, kann auf "Services" nun die SAP Web IDE ausgewählt werden, mit "Got to Service" gelangt man direkt dort hin. <br><br>

Nun kann man unter Files > New > Project from Template eine SAPUI5 Application erstellen. In diesem Tutorial wählt man am besten als View Type "XML" und als View Name "Home" und drückt anschließend auf "Finish". ![Template](template.png)<br><br>

Mit einem Rechtsklick auf das erstellte Projekte kann man dieses exportieren. Es empfiehlt sich auf einer IDE außerhalb der SAPWebIDE zu arbeiten, da wir den Node.js-Server mit Middleware und MySQL-Anbindung installieren wollen. Anschließend soll die Zip noch entpackt werden. <br>Optional kann die App testweise sofort mit Alt+F5 gestartet werden. ![Export](export.png)<br><br>

Unter Webstorm wollen wir nun den Server einrichten und die Export-Zip integrieren. Nach dem Öffnen von Webstorm in Ubuntu unter Application > Programming > Webstorm > Open wählt man das entsprechende "Export-Projekt". ![Open](open.png)<br><br> 
In der IDE sehen wir nun folgende Struktur:<br> ![Struktur](struktur.png)<br><br>

### 1.2. Installieren der NPM- und SAP-Module sowie des Node.js-Servers
Weiter geht es im Ordner <code>~/home/Template/UI5_App/</code>. Hier finden wir zwei Ordner: "UI5_HANA_APL_server" und UI5_HANA_APL".

In "UI5_HANA_APL_server" installieren wir einen Node.js-Server und legen dabei einen neuen Ordner namens "TestProjectUI5Server" an. Im Ordner installieren wir nun alle benötigten Node-Module.<br>
Zuerst legen wir eine <i>package.json</i> an: <br>

<code>
{<br>
&nbsp;&nbsp;&nbsp;&nbsp;"name": "ui5hanaaplbackend",<br>
&nbsp;&nbsp;&nbsp;&nbsp;"version": "1.0.0",<br>
&nbsp;&nbsp;&nbsp;&nbsp;"main": "server.js",<br>
&nbsp;&nbsp;&nbsp;&nbsp;"scripts": {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"test": "echo \"Error: no test specified\" && exit 1",<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"start": "node server.js"<br>
&nbsp;&nbsp;&nbsp;&nbsp;},<br>
&nbsp;&nbsp;&nbsp;&nbsp;"author": "",<br>
&nbsp;&nbsp;&nbsp;&nbsp;"license": "ISC",<br>
&nbsp;&nbsp;&nbsp;&nbsp;"dependencies": {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"express": "^4.15.4",<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mysql": "^2.15.0"<br>
&nbsp;&nbsp;&nbsp;&nbsp;},<br>
&nbsp;&nbsp;&nbsp;&nbsp;"devDependencies": {},<br>
&nbsp;&nbsp;&nbsp;&nbsp;"description": ""<br>
}
</code><br><br>

In der Konsole (Applications > System Tools > Terminator) im Ordner des TestProjectUI5Servers geben wir <code>npm init && npm install --save-dev</code> ein. Die Node-Module des Servers sollten nun im Ordner installiert sein.<br><br>
Nun tragen wir noch folgenden Code in eine Datei names <i>server.js</i> folgendes ein:<br>
<code>
// Test server

var express = require('express');<br>
var mysql = require('mysql');

// init backend app<br>
var app = express();

// database definition<br>
var connection = mysql.createConnection({<br>
&nbsp;&nbsp;&nbsp;&nbsp;host: 'localhost',<br>
&nbsp;&nbsp;&nbsp;&nbsp;user: 's72898',<br>
&nbsp;&nbsp;&nbsp;&nbsp;password: 's72898',<br>
&nbsp;&nbsp;&nbsp;&nbsp;database: 'apl_ui5_testdb'<br>
});

// connect to mysql testdb<br>
connection.connect(function(err) {<br>
&nbsp;&nbsp;&nbsp;&nbsp;if(err) throw err;<br>
&nbsp;&nbsp;&nbsp;&nbsp;console.log('Connected...');<br>
});

app.post('/getentries', function(request, response) {<br>
&nbsp;&nbsp;&nbsp;&nbsp;var mySqlQuery = request.query.mysqlquery; <br>
&nbsp;&nbsp;&nbsp;&nbsp;var resultSet = [];

&nbsp;&nbsp;&nbsp;&nbsp;connection.query(mySqlQuery, function(err, rows, fields) {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if(err) {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;console.log(err);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;resultSet = err;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} else {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for(var i in rows) {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;console.log(rows[i]);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;resultSet.push(rows[i]);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;response.writeHead(200, "OK", {'Content-Type': 'text/plain'});<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;response.write(JSON.stringify(resultSet));<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;response.end();<br>
&nbsp;&nbsp;&nbsp;&nbsp;});<br>
});

app.use(express.static('../UI5_HANA_APL'));

app.listen(3000, function() {<br>
&nbsp;&nbsp;&nbsp;&nbsp;console.log('Webserver listening on port 3000...');<br>
});

</code><br>

In <code>~/home/Template/UI5_App/UI5_HANA_APL_server</code> starten wir die App testweise mit <code>nodemon server.js</code>:<br><br> ![RunServer](runserver.png)
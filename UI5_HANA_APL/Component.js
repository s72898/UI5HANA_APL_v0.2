sap.ui.define([
    "sap/ui/core/UIComponent"
], function(UIComponent) {
    // private
    var _oComponentId = "sap.ui.UI5_HANA_APL.Component";

    // public
    var OComponent = {
        metadata : {
            // manifest definition instead of 'rootView: "sap.ui.demo.wt.view.App"'
            manifest: "json"
        },
        init : function () {
            // call the init function of the parent
            UIComponent.prototype.init.apply(this, arguments);

            this.getRouter().initialize();
        }
    };

    return UIComponent.extend(_oComponentId, OComponent);
});


sap.ui.define([
    'sap/ui/core/mvc/Controller'
], function(Controller) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.Home';

    var OController = {
        constructor: function () {}
    };

    return Controller.extend(_oControllerId, OController);
});
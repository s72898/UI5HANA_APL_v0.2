sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    'sap/ui/UI5_HANA_APL/util/control/TableFactory'
], function(Controller, JSONModel, TableFactory) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.TableUI';
    var self;
    var TABLE_ID = 'action_table_panel';

    var OController = {
        onInit: function() {
            self = this;
            //TableModel.initializeOTable(self, TABLE_ID);
            new TableFactory().createDefaultOTable.call(self, TABLE_ID);
        },
        updateTable: function(json) {
            //TableModel.updateOTable(self, json, TABLE_ID);
            new TableFactory().updateOTable.call(self, json);
        },
        constructor: function() {}
    };

    return Controller.extend(_oControllerId, OController);
});
sap.ui.define([
    "sap/ui/core/Control",
    'sap/ui/core/routing/History'
], function(Control, History) {
    "use strict";

    // public
    var OControl = {
        goToPrevPage: function(oRouter, oPage) {
            var oHistory, sPreviousHash;

            oHistory = History.getInstance();
            sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                oRouter.navTo(oPage, {}, true);
            }
        }
    };

    return Control.extend('sap.ui.UI5_HANA_APL.ui5_hana_apl.util.control.NavToPrevious', OControl);
});
